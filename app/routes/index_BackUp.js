'use strict';

const Router = require('express-async-router').AsyncRouter;
const { reply } = require('node-weixin-message');
const MongoClient = require('mongodb').MongoClient;
const router = Router();
const io = require('socket.io-client');
var onlineC=[], liveStatus={};
// GET /
// Verify server avaliablility
router.get('/', (req, res) => {
  const { echostr } = req.query;
  console.log("req.query::: ",req.query);
  res.send(echostr);
});

// POST /
// Handler all incoming message from wechat
router.post('/', async (req, res) => {

  const { message, chatContext } = req;
  const { chat } = req.app.locals;
  console.log("in route/index::: ",req.user);
  const { output: { text }, context } = await chat(message.Content, chatContext);
  if (onlineC.indexOf(message.FromUserName) == -1){
    console.log("new client");
    onlineC.push(message.FromUserName);
    liveStatus[message.FromUserName] = false;
    const socket = io('http://ces-server.ap-southeast-2.elasticbeanstalk.com:3001');
    socket.on('connecting', function (){
    	console.log('socket connecting');
    });
    socket.on('connect', function (){
      console.log('connected');
      socket.emit('new client', "Client_"+message.FromUserName, "Wechat");
    });
  }
  const storage = req.sessionStore;
  var clientSent = message;
  var date = new Date();
  clientSent.CreateTime = date.toLocaleString();
  clientSent.conversation_id = context.conversation_id;

  storage.set(req.user, context);

  var req = {
			'addresser':"Client_"+message.FromUserName,
			'recipient':'',
			'type':'plain',
			'body':message.Content
		}
	socket.emit('send public message', req);

  //////   User Message   //////
  // MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
  //   const db = client.db("ChatRecord");
  //   db.collection(message.FromUserName).insert(clientSent, function(err, results){
  //     if (err) {
  //       console.log(err);
  //       db.createCollection(message.FromUserName,function(err,result){
  //         if (err) console.log(err);
  //         db.collection(message.FromUserName).insert(clientSent, function(err, results){
  //           if (err) console.log(err);
  //         //  console.log(results);
  //           client.close();
  //         })
  //       });
  //     }
  //     client.close();
  //   });
  // });
// console.log("chatContext::: ", context.conversation_id);
  const response = reply.text(message.ToUserName, message.FromUserName, text[0]);
  response.json.conversation_id = context.conversation_id;
//  console.log("responsejson::: ", response.json);
  // var responsejson = xmlToJson(response);

  socket.emit("send chatbot message",{
			'recipient': 'Client_' + message.FromUserName,
			'body': text[0]
	});

  //////   WATSON REPLY   //////
  // MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
  //   const db = client.db("ChatRecord");
  //   db.collection(message.FromUserName).insert(response.json, function(err, results){
  //     if (err) console.log(err);
  //     client.close();
  //   });
  // });
  res.set('Content-Type', 'text/xml');
  res.send(response.xml);
});



module.exports = router;
