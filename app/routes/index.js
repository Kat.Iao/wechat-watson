'use strict';

const config = require('../config');
const Router = require('express-async-router').AsyncRouter;
const { reply } = require('node-weixin-message');
const settings = require('node-weixin-settings');
const { service } = require('node-weixin-message');
const { getInfo } = require('node-weixin-message');
const MongoClient = require('mongodb').MongoClient;
const request = require('request');
const router = Router();
const io = require('socket.io-client');
const socket = io('http://CesServer.daocloudapp.com');
// const socket = io('https://katces.localtunnel.me/')
var onlineC=[], csocket={}, liveStatus={}, liveMes={}, liveAgent={};
socket.on('connecting', function (){
  console.log('socket connecting');
});
socket.on('connect', function (){
  console.log('connected');
});
socket.on('agent received', (data) => {
  console.log(data);
  if (data.status == "takeover"){
  console.log("agent received ",data.recipient);
  liveStatus[data.recipient] = true;
  liveAgent[data.recipient] = data.addresser;
}else if(data.status == "hangup"){
  console.log("agent hang up ",data.recipient);
  liveStatus[data.recipient] = false;
}
});
socket.on('receive private message', (data) => {
    console.log("text::: ", data.body);
    liveMes[data.recipient] = data.body;
    // var options ={
    //   uri: 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=23_nE7zAM1BphGVDd5l2i_XAOLdN-CBRottPTfGsEfXBsM9h6u7CDGl71dtcZwBuntWuYR2Px5qu4CJ2MHNyyv5jaBuMVqwNVVbx3_tpOSq-oIrWKzqof073_PD2qyFggAbUKl39Qt0KAibQKhhMKHiADACAX',
    //   method: 'POST',
    //   json:{
    //     "touser":data.recipient.substring(7),
    //     "msgtype":"text",
    //     "text":
    //     {
    //          "content":data.body
    //     }
    //   }
    // };
    // request(options, function(err, res, body){
    //   if (err) console.log(err);
    //   // console.log("res::", res);
    //   console.log("body:: ", body);
    // })

    service.api.text(settings, config.app, data.recipient.substring(7), data.body, function(err, data){
      if (err) console.log(err);
      console.log(data);
    })
});
socket.on('base64 file', (msg) => {
  console.log("received file");

});

// GET /
// Verify server avaliablility
router.get('/', (req, res) => {
  const { echostr } = req.query;
  //console.log("req.query::: ",req.query);
  res.send(echostr);
});

// POST /
// Handler all incoming message from wechat
router.post('/', async (req, res) => {
// console.log("req::: ", req);
 // console.log("res::: ", res);
  const { message, chatContext } = req;
  const { chat } = req.app.locals;
  var icon;
  if (onlineC.indexOf(message.FromUserName) == -1){
    console.log("new client::: ", message.FromUserName);
//    getInfo.getUser.info(settings, config.app, message.FromUserName, function(err, data){
//      if (err) {
//        console.log(err);
        socket.emit('new client', "Wechat_"+message.FromUserName, "Wechat", message.FromUserName);
//      };
//      icon = (JSON.parse(data)).headimgurl;
//      socket.emit('new client', "Wechat_"+message.FromUserName, "Wechat",message.FromUserName, icon);
//    })

    // var options ={
    //   url: 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=23_nE7zAM1BphGVDd5l2i_XAOLdN-CBRottPTfGsEfXBsM9h6u7CDGl71dtcZwBuntWuYR2Px5qu4CJ2MHNyyv5jaBuMVqwNVVbx3_tpOSq-oIrWKzqof073_PD2qyFggAbUKl39Qt0KAibQKhhMKHiADACAX&openid=ovHp155hGRz8x0oEWQ8DT5mYaV1k&openid='+message.FromUserName,
    //   form:{}
    // };
    // request.get(options, function(err, res, body){
    //   if (err) console.log(err);
    //   // console.log("res::", res);
    //   icon = body.headimgurl;
    //   console.log("body:: ", body);
    // })

    onlineC.push(message.FromUserName);
  //  csocket[message.FromUserName] = socket;
    liveStatus['Wechat_'+message.FromUserName] = false;
  }

//  var socket;
  //console.log("in route/index::: ",req.user);
  const { output: { text }, context } = await chat(message.Content, chatContext);
    // console.log("Chat context::: ", context);
    // console.log("chatContext::: ", chatContext);
    //////  context == chatContext //////
  const storage = req.sessionStore;
  storage.set(req.user, context);

  //////   User Message   //////
  // MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
  //   const db = client.db("ChatRecord");
  //   db.collection(message.FromUserName).insert(clientSent, function(err, results){
  //     if (err) {
  //       console.log(err);
  //       db.createCollection(message.FromUserName,function(err,result){
  //         if (err) console.log(err);
  //         db.collection(message.FromUserName).insert(clientSent, function(err, results){
  //           if (err) console.log(err);
  //         //  console.log(results);
  //           client.close();
  //         })
  //       });
  //     }
  //     client.close();
  //   });
  // });
// console.log("chatContext::: ", context.conversation_id);
  if (liveStatus['Wechat_'+message.FromUserName]){
    console.log(icon);
    var msg = {
        'addresser':"Wechat_"+message.FromUserName,
        'recipient':liveAgent['Wechat_'+message.FromUserName],
        'type':'plain',
        'body':message.Content,
  //      'icon':icon
      };
    socket.emit('send private message', msg);
    const response = reply.text(message.ToUserName, message.FromUserName, '');
    res.set('Content-Type', 'text/xml');
    res.send('');
    // res.send(200);
    liveMes['Wechat_'+message.FromUserName]='';
  }else{
    console.log(icon);
    var msg = {
  			'addresser':"Wechat_"+message.FromUserName,
  			'recipient':'',
  			'type':'plain',
  			'body':message.Content,
//        'icon':icon
  		};
  	socket.emit('send public message', msg);
    const response = reply.text(message.ToUserName, message.FromUserName, text[0]);
    console.log('response::: ', response);
    // response.json.conversation_id = context.conversation_id;
  //  console.log("responsejson::: ", response.json);
    // var responsejson = xmlToJson(response);
    console.log("text:: ",text[0]);
    socket.emit("send chatbot message",{
        'recipient': 'Wechat_' + message.FromUserName,
        'body': text[0]
    });
    if (text[1] && text[1] == 'K999'){
      socket.emit("new request", 'Wechat_' + message.FromUserName);
      console.log("calling for live chat");
    }
    if(text[0]=='我没理解您的意思' || text[0]=='您能改个说法吗？我没听懂' || text[0]=='我没听懂。请换个讲法再说一次'){
      socket.emit('misunderstanding', 'Wechat_' + message.FromUserName);
      MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
         if (err) console.log(err);
         var doc = {
           client_id: "Wechat_"+message.FromUserName,
           occurtime: new Date(),
           input: message.Content,
           output: text[0]
         }
         client.db('DataRecord').collection('improvable').insert(doc, function(err, results){
           if (err) console.log(err);
           console.log(results);
           client.close();
         })
       })
    }else{
      socket.emit('understanding', 'Wechat_' + message.FromUserName);
    }

    //////   WATSON REPLY   //////
    // MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
    //   const db = client.db("ChatRecord");
    //   db.collection(message.FromUserName).insert(response.json, function(err, results){
    //     if (err) console.log(err);
    //     client.close();
    //   });
    // });
    console.log('res.send::: ', response);
    res.set('Content-Type', 'text/xml');
    res.send(response);
  }


});



module.exports = router;
